#ifndef HEADER_FENPRINCIPALE
#define HEADER_FENPRINCIPALE

#include <QtWidgets>
#include <vector>
#include <QtWebEngineWidgets>

const static QString ICONES = "../znavigo_icones/";

class FenPrincipale : public QMainWindow
{
    Q_OBJECT

public:
    FenPrincipale();
    virtual ~FenPrincipale();

    public slots:
    void chargePage();
    void prec();
    void suiv();
    void recharger();
    void pageChargee(int progres);
    void nouvelOnglet(int index);
    void nouvelOnglet();
    void changeOnglet(int index);
    void fermerOnglet(int index);
    void fermerOnglet();
    void arreterChargement();
    void retourAccueil();
    void dialogZ();
    void dialogQt();

    private:
    QAction *precedent, *suivant, *actualiser, *accueil, *lancer, *arreter, *quitter, *apropos, *aproposqt, *nOnglet, *fOnglet;
    QMenu *fichier, *navigation, *aide;
    QToolBar *barre;
    QProgressBar *progression;
    QLineEdit *saisie;
    QWebEngineView *pageactuelle;

    QTabWidget *onglets;

    void setActions();
    void setMenus();
    void setToolbar();
    void setOnglets();
    void setStatusBar();
    void connections();
    void ajouterOnglet(QWebEngineView *page);
};

#endif