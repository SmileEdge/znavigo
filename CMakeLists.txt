cmake_minimum_required(VERSION 3.13)
project(zNavigo)
set(CMAKE_CXX_STANDARD 14)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_PREFIX_PATH "PATH_TO_QT/QT_VERSION/QT_ARCH/lib/cmake")

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5WebKitWidgets REQUIRED)
find_package(Qt5WebEngine REQUIRED)
find_package(Qt5WebEngineWidgets REQUIRED)

add_executable(zNavigo main.cpp FenPrincipale.h FenPrincipale.cpp)

target_link_libraries(zNavigo Qt5::Core)
target_link_libraries(zNavigo Qt5::Widgets)
target_link_libraries(zNavigo Qt5::Quick)
target_link_libraries(zNavigo Qt5::WebKitWidgets)
target_link_libraries(zNavigo Qt5::WebEngine)
target_link_libraries(zNavigo Qt5::WebEngineWidgets)
